from django.urls import path

from . import views

urlpatterns = [
    path("exchange", views.config_exchanges),
    path("user-mailboxes", views.list_user_mailboxes),
    path("user-mailboxes/create", views.modal_user_mailboxes_create),
    path("user-mailbox/<str:mailbox>", views.user_mailbox_edit),
    path("user-mailbox/<str:mailbox>/delete", views.modal_user_mailbox_delete),
    path("resource-mailboxes", views.list_resource_mailboxes),
    path("resource-mailboxes/create", views.modal_resource_mailboxes_create),
    path("resource-mailbox/<str:mailbox>", views.resource_mailbox_edit),
    path("resource-mailbox/<str:mailbox>/delete", views.modal_resource_mailbox_delete),
    path("team", views.config_team),
    path("meet", views.config_meet),
    path("filesharing", views.config_filesharing),
    path("archiving", views.config_archiving),
]
