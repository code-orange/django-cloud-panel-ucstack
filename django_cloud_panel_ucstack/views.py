from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from django.template import loader
from django.utils.translation import gettext as _

from django_cloud_panel_main.django_cloud_panel_main.views import get_nav
from django_session_ldap_attributes.django_session_ldap_attributes.decorators import (
    admin_group_required,
)


@admin_group_required("UC")
def config_exchanges(request):
    template = loader.get_template("django_cloud_panel_ucstack/config_exchange.html")

    template_opts = dict()

    template_opts["content_title_main"] = _("Unified Communications")
    template_opts["content_title_sub"] = _("Exchanges Overview")

    template_opts["menu_groups"] = get_nav(request)

    return HttpResponse(template.render(template_opts, request))


@admin_group_required("UC")
def list_user_mailboxes(request):
    template = loader.get_template(
        "django_cloud_panel_ucstack/list_user_mailboxes.html"
    )

    template_opts = dict()

    template_opts["content_title_main"] = _("Unified Communications")
    template_opts["content_title_sub"] = _("Mailboxes Overview")

    template_opts["menu_groups"] = get_nav(request)

    template_opts["all_user_mailboxes"] = list()

    return HttpResponse(template.render(template_opts, request))


@admin_group_required("UC")
def user_mailbox_edit(request, mailbox):
    template = loader.get_template("django_cloud_panel_ucstack/edit_user_mailbox.html")

    template_opts = dict()

    template_opts["content_title_main"] = _("Unified Communications")
    template_opts["content_title_sub"] = _("Mailbox Edit") + " " + mailbox

    template_opts["menu_groups"] = get_nav(request)

    template_opts["mailbox"] = mailbox

    return HttpResponse(template.render(template_opts, request))


@admin_group_required("UC")
def modal_user_mailbox_delete(request, mailbox):
    template = loader.get_template(
        "django_cloud_panel_ucstack/modal_user_mailbox_delete.html"
    )

    template_opts = dict()

    template_opts["content_title_main"] = _("Unified Communications")
    template_opts["content_title_sub"] = _("Mailbox Delete") + " " + mailbox

    template_opts["menu_groups"] = get_nav(request)

    template_opts["mailbox"] = mailbox

    return HttpResponse(template.render(template_opts, request))


@admin_group_required("UC")
def modal_user_mailboxes_create(request):
    template = loader.get_template(
        "django_cloud_panel_ucstack/modal_user_mailboxes_create.html"
    )

    template_opts = dict()

    template_opts["content_title_main"] = _("Unified Communications")
    template_opts["content_title_sub"] = _("Mailbox Create")

    template_opts["menu_groups"] = get_nav(request)

    return HttpResponse(template.render(template_opts, request))


@admin_group_required("UC")
def list_resource_mailboxes(request):
    template = loader.get_template(
        "django_cloud_panel_ucstack/list_resource_mailboxes.html"
    )

    template_opts = dict()

    template_opts["content_title_main"] = _("Unified Communications")
    template_opts["content_title_sub"] = _("Mailboxes Overview")

    template_opts["menu_groups"] = get_nav(request)

    template_opts["all_resource_mailboxes"] = list()

    return HttpResponse(template.render(template_opts, request))


@admin_group_required("UC")
def resource_mailbox_edit(request, mailbox):
    template = loader.get_template(
        "django_cloud_panel_ucstack/edit_resource_mailbox.html"
    )

    template_opts = dict()

    template_opts["content_title_main"] = _("Unified Communications")
    template_opts["content_title_sub"] = _("Mailbox Edit") + " " + mailbox

    template_opts["menu_groups"] = get_nav(request)

    template_opts["mailbox"] = mailbox

    return HttpResponse(template.render(template_opts, request))


@admin_group_required("UC")
def modal_resource_mailbox_delete(request, mailbox):
    template = loader.get_template(
        "django_cloud_panel_ucstack/modal_resource_mailbox_delete.html"
    )

    template_opts = dict()

    template_opts["content_title_main"] = _("Unified Communications")
    template_opts["content_title_sub"] = _("Mailbox Delete") + " " + mailbox

    template_opts["menu_groups"] = get_nav(request)

    template_opts["mailbox"] = mailbox

    return HttpResponse(template.render(template_opts, request))


@admin_group_required("UC")
def modal_resource_mailboxes_create(request):
    template = loader.get_template(
        "django_cloud_panel_ucstack/modal_resource_mailboxes_create.html"
    )

    template_opts = dict()

    template_opts["content_title_main"] = _("Unified Communications")
    template_opts["content_title_sub"] = _("Mailbox Create")

    template_opts["menu_groups"] = get_nav(request)

    return HttpResponse(template.render(template_opts, request))


@admin_group_required("UC")
def config_team(request):
    template = loader.get_template("django_cloud_panel_ucstack/config_team.html")

    template_opts = dict()

    template_opts["content_title_main"] = _("Unified Communications")
    template_opts["content_title_sub"] = _("Team Overview")

    template_opts["menu_groups"] = get_nav(request)

    return HttpResponse(template.render(template_opts, request))


@admin_group_required("UC")
def config_meet(request):
    template = loader.get_template("django_cloud_panel_ucstack/config_meet.html")

    template_opts = dict()

    template_opts["content_title_main"] = _("Unified Communications")
    template_opts["content_title_sub"] = _("Conference Overview")

    template_opts["menu_groups"] = get_nav(request)

    return HttpResponse(template.render(template_opts, request))


@admin_group_required("UC")
def config_filesharing(request):
    template = loader.get_template("django_cloud_panel_ucstack/config_filesharing.html")

    template_opts = dict()

    template_opts["content_title_main"] = _("Unified Communications")
    template_opts["content_title_sub"] = _("Filesharing Overview")

    template_opts["menu_groups"] = get_nav(request)

    return HttpResponse(template.render(template_opts, request))


@admin_group_required("UC")
def config_archiving(request):
    template = loader.get_template("django_cloud_panel_ucstack/config_archiving.html")

    template_opts = dict()

    template_opts["content_title_main"] = _("E-Mail")
    template_opts["content_title_sub"] = _("Archive Overview")

    template_opts["menu_groups"] = get_nav(request)

    return HttpResponse(template.render(template_opts, request))
